---
layout: markdown_page
title: "Plan Stage"
---

## On this page
{:.no_toc}

- TOC
{:toc}

### Plan
{: #welcome}

Plan Teams By Function: [Frontend Engineering](/handbook/engineering/frontend/plan/) • [Backend Engineering](/handbook/engineering/development/dev/plan/)

The responsibilities of this collective team are described by the [Plan stage](/handbook/product/categories/#plan-stage). Among other things, this means 
working on GitLab's functionality around issues, issue boards, milestones, todos, issue lists and filtering, roadmaps, time tracking, requirements management, and notifications.

* I have a question. Who do I ask?

In GitLab issues, questions should start by @ mentioning the Product Manager for the [corresponding Plan stage group](/handbook/product/categories/#plan-stage). GitLab team-members can also use [#g_plan](https://gitlab.slack.com/messages/C72HPNV97).

### How we work

* In accordance with our [GitLab values](/handbook/values/)
* Transparently: nearly everything is public, we record/livestream meetings whenever possible
* We get a chance to work on the things we want to work on
* Everyone can contribute; no silos
* We do an optional, asynchronous daily stand-up in [#g_plan_standup](https://gitlab.slack.com/messages/CF6QWHRUJ).

### Workflow

We work in a continuous Kanban manner while still aligning with Milestones and [GitLab's Product Development Flow](/handbook/product-development-flow/).

#### Issues

Issues have the following lifecycle:

```mermaid
graph LR;
  A[Open] --> Y;
  Y["Triage: Backlog OR Awaiting Further Demand OR Close"] --> Z;
  Z[Validation Backlog] --> B;
  B[Problem Validation] --> C;
  C[Solution Validation] --> D;
  D[Planning Breakdown] --> E;
  E[To Schedule] --> F;
  F[Release Backlog] --> G;
  G[In Dev] --> H;
  H[In Review] --> I;
  I[In Verify] --> J;
  J[Launch] --> K;
  K[Learn] --> L[Next Iteration];
```

#### Epics
If an issues is `> 3 weight`, split it up into multiple issues. After you split issues into vertical feature slices, create an epic with the appropriate labels - `devops::plan`, `group::*`, `Category::*` or `feature label` - and attach all of the stories that represent the larger feature. This will help represent the larger effort on the roadmap and make it easier to schedule. 

#### Roadmap Organization

```mermaid
graph TD;
  A["devops::plan"] --> B["group::*"];
  B --> C["category::*"];
  B --> D["non-category feature"];
  C --> E["maturity::minimal"];
  C --> F["maturity::viable"];
  C --> G["maturity::complete"];
  C --> H["maturity::lovable"];
  E--> I["Iterative Epic(s)"];
  F--> I;
  G --> I;
  H --> I;
  D --> I;
  I--> J["Issues"];
```

#### Links / References

`~devops::plan` - [Kanban Board](https://gitlab.com/groups/gitlab-org/-/boards/1226305?&label_name[]=devops%3A%3Aplan) • [Roadmap](https://gitlab.com/groups/gitlab-org/-/roadmap?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Aplan)

`~group::team planning` - [Kanban Board](https://gitlab.com/groups/gitlab-org/-/boards/1235826?&label_name[]=devops%3A%3Aplan&label_name[]=group%3A%3Ateam%20planning) • [Roadmap](https://gitlab.com/groups/gitlab-org/-/roadmap?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Aplan&label_name[]=group%3A%3Ateam%20planning)

`~group::portfolio management` - [Kanban Board](https://gitlab.com/groups/gitlab-org/-/boards/1224651?&label_name[]=devops%3A%3Aplan&label_name[]=group%3A%3Aportfolio%20management) • [Roadmap](https://gitlab.com/groups/gitlab-org/-/roadmap?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Aplan&label_name[]=group%3A%3Aportfolio%20management)

`~group::certify` - [Kanban Board](https://gitlab.com/groups/gitlab-org/-/boards/1235846?&label_name[]=devops%3A%3Aplan&label_name[]=group%3A%3Acertify) • [Roadmap](https://gitlab.com/groups/gitlab-org/-/roadmap?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Aplan&label_name[]=group%3A%3Acertify)


#### Process Improvements We're Working Towards

* **Smaller Iterations:** A 4-6 weeks release cycle is helpful from a marketing perspective, but it is too long of a time horizon for continuously delivering working software incrementally and iteratively. We can accomplish this by breaking bigger features intto smaller vertical feature slices. If an Issue has a `weight` of `> 2`, it is a good signal it should be [broken down into smaller pieces](https://www.youtube.com/watch?v=EDT0HMtDwYI). The larger the weight, the more risk and likelihood the Issue will take longer than the estimate.
* **Limit Work In Progress:** [Context switching is expensive](https://blog.codinghorror.com/the-multi-tasking-myth/). By limiting the amount of Issues in progress at any given time, the less frequently you will have to incur this cost. You may think multitasking is inevitable, but by adhering to a work in progress limit, you will more quickly surface the parts of our process that are inefficient; enabling us to collectively fix them as a team.